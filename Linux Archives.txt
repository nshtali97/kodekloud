On Nautilus storage server in Stratos DC there is a storage location /data which is used by different developers to keep their data (no confidential data). One of the developers john has raised a ticket and asked for a copy of his/her data present in /data/john directory on storage server. /home is an FTP location on storage server where developers can download their data. Below are the instructions shared by the system admin team to accomplish the task:


a. Make a john.tar.gz compressed archive of /data/john directory and move the archive to /home directory on Storage Server.


Solutions:

1) Login to Storage Server

ssh natasha@ststor01

2) Get out from user folder

cd ..
ls
cd ..

3) Check the location

ls

4) Compress the data/john folder

sudo tar -cvzf john.tar.gz /data/john

5) Move to /home folder

sudo mv john.tar.gz /home