xFusionCorp Industries is planning to host two static websites on their infra in Stratos Datacenter. The development of these websites is still in -progress, but we want to get the servers ready. The storage server has a shared directory /data that is mounted on each app host under /var/www/html directory. Please perform the following steps to accomplish the task:


a. Install httpd package and dependencies on all app hosts.

b. Apache should serve on port 8080 within the apps.

c. There are two website's backups /home/thor/beta and /home/thor/demo on jump_host. Set them up on Apache in a way that beta should work on link http://<<lb-url>>/beta/ and demo should work on link http://<<lb-url>>/demo. (do not worry about load balancer configuration, as its already configured).

d. You can access the website on LBR link; to do so click on the + button on top of your terminal, select the option Select port to view on Host 1, and after adding port 80 click on Display Port.


SOlutions:

1) Login to each & every AppServer separately 

  ssh tony@stapp01
  ssh steve@stapp02
  ssh banner@stapp03

2) Login as root
  
  sudo su -

3) Install httpd and OpenSSH in all AppServers

  yum install httpd -y
  yum install openssh-clients -y
  systemctl restart sshd

4) Copy mentioned folders in the question  to every appserver from jump host.

  scp -r /home/thor/beta tony@stapp01:/tmp
  scp -r /home/thor/demo tony@stapp01:/tmp

  scp -r /home/thor/beta steve@stapp02:/tmp
  scp -r /home/thor/demo steve@stapp02:/tmp

  scp -r /home/thor/beta banner@stapp03:/tmp
  scp -r /home/thor/demo banner@stapp03:/tmp

5) Then from any appserver move the folders to server html folder

  mv /tmp/beta /var/www/html
  mv /tmp/demo /var/www/html

6) change Listen port to 8080 and restart httpd in all appservers.
  
  vi /etc/httpd/conf/httpd.conf
 
  systemctl restart httpd
 
7) Check from jump host

   curl http://stapp01:8080/beta/
   curl http://stapp01:8080/demo/

   curl http://stapp02:8080/beta/
   curl http://stapp02:8080/demo/

   curl http://stapp03:8080/beta/
   curl http://stapp03:8080/demo/ 